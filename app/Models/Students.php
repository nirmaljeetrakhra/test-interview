<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Students extends Model
{
    use HasFactory;
    protected $fillable = [
        'first_name',
        'last_name',
        'universitie_id',
        'universitie_id',
        'email',
        'phone',
    ];
    public function students()
    {
        return $this->belongsTo(Universities::class);
    }
}
