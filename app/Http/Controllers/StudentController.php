<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Students;
use App\Models\Universities;
use Validator;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('student',[
            'student' => Students::paginate(5),
            'uni' => Universities::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'first_name'=>'required',
            'last_name'=>'required',
            // 'phone'=>'required',
            'email' => ['required','unique:students','email'],
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'message' => $validator->errors()->first()],200);
        }
        if(Students::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'phone' => $request->phone ?? '',
            'universitie_id' => $request->universitie_id,
        ]))
        {
            return response()->json(['success' => true, 'message' => 'Added'],200);
        }
        return response()->json(['success' => false, 'message' => 'We are facing some technical issues'],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $rules = [
            'first_name'=>'required',
            'last_name'=>'required',
            'email' => ['required','email'],
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'message' => $validator->errors()->first()],200);
        }
        if(Students::where([['email',$request->email],['id','!=',$request->id]])->count() != 0)
        {
            return response()->json(['success' => false, 'message' => 'Email already taken'],200);
        }
        $student = Students::find($request->id);
        $update = [
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'phone' => $request->phone ?? '',
            'universitie_id' => $request->universitie_id,
        ];
        if($student->update($update))
        {
            return response()->json(['success' => true, 'message' => 'Updated'],200);
        }
        return response()->json(['success' => false, 'message' => 'We are facing some technical issues'],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try {
            //code...
            Students::destroy($id);
            return response()->json(['success' => true, 'message' => 'Deleted'],200);
        } catch (\Throwable $th) {
            //throw $th;
            return response()->json(['success' => false, 'message' => $th->getMessage()],200);
        }
    }
}
