<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // return view('welcome');
    return redirect()->route('home');
});
Auth::routes(['register' => false]);
Route::group(['middleware' => 'auth'],function(){
    Route::get('/home', [App\Http\Controllers\UniversityController::class, 'index'])->name('home');
    Route::post('/add-uni', [App\Http\Controllers\UniversityController::class, 'store'])->name('uni.add');
    Route::post('/uni-update', [App\Http\Controllers\UniversityController::class, 'update'])->name('uni.update');
    Route::delete('/uni-delete/{id}', [App\Http\Controllers\UniversityController::class, 'destroy'])->name('uni.delete');

    Route::get('/student', [App\Http\Controllers\StudentController::class, 'index'])->name('student');
    Route::post('/add-st', [App\Http\Controllers\StudentController::class, 'store'])->name('st.add');
    Route::post('/st-update', [App\Http\Controllers\StudentController::class, 'update'])->name('st.update');
    Route::delete('/st-delete/{id}', [App\Http\Controllers\StudentController::class, 'destroy'])->name('st.delete');

});
