<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Universities extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'email',
        'logo',
        'website'
    ];
    public function students()
    {
        return $this->belongsToMany(Students::class);
    }
    public function getLogoAttribute()
    {
        if($this->attributes['logo'])
        {
            return asset('storage/'.str_replace('public/','',$this->attributes['logo']));
        }
        return '';
    }
}
