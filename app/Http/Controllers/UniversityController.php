<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Models\Universities;
use Mail;

class UniversityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        // return ';;';
        return view('universities',[
            'unies' => Universities::paginate(5)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
       //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rules = [
            'name'=>'required',
            'email' => ['required','unique:universities','email'],
            'website' => ['required','url'],
            'logo' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'message' => $validator->errors()->first()],200);
        }
        $logo = $request->file('logo')->store('/public/images');
        if(Universities::create([
            'name' => $request->name,
            'email' => $request->email,
            'website' => $request->website,
            'logo' => $logo,
        ]))
        {
            Mail::send('email', ['name' => $request->name ?? ''], function($message) {
                $message->to('matrixmtestcadidate@mailinator.com', 'matrix')
                ->subject('New University added');
            });
            return response()->json(['success' => true, 'message' => 'Added'],200);
        }
        return response()->json(['success' => false, 'message' => 'We are facing some technical issues'],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $rules = [
            'name'=>'required',
            'email' => ['required','email'],
            'website' => ['required','url'],
            // 'logo' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'message' => $validator->errors()->first()],200);
        }
        if(Universities::where([['email',$request->email],['id','!=',$request->id]])->count() != 0)
        {
            return response()->json(['success' => false, 'message' => 'Email already taken'],200);
        }
        $uni = Universities::find($request->id);
        $update = [
            'name' => $request->name,
            'email' => $request->email,
            'website' => $request->website,
        ];
        if($request->file('logo'))
        {
            $update['logo'] = $request->file('logo')->store('/public/images');
        }
        if($uni->update($update))
        {
            return response()->json(['success' => true, 'message' => 'Updated'],200);
        }
        return response()->json(['success' => false, 'message' => 'We are facing some technical issues'],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try {
            //code...
            Universities::destroy($id);
            return response()->json(['success' => true, 'message' => 'Deleted'],200);
        } catch (\Throwable $th) {
            //throw $th;
            return response()->json(['success' => false, 'message' => 'Cannot delete university as it has students data'],200);
        }
    }
}
