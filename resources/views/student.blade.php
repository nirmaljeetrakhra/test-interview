@extends('layouts.main')

@section('content')
    <div class="container">
        <!-- Button to Open the Modal -->
        <div>
            <h2>Student</h2>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                Add New
            </button>
        </div>
        <br />
        <!-- university tabel -->
        <table class="table">
            <thead>
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>University</th>
                <th>phone</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($student as $kk => $u)
                <tr>
                    <td>{{$u->first_name}}</td>
                    <td>{{$u->last_name}}</td>
                    <td>{{$u->email}}</td>
                    <td>{{$u->universitie_id}}</td>
                    <td>{{$u->phone}}</td>
                    <td>
                        <span data-toggle="modal" data-target="#edit-modal-{{ $u->id }}" >Edit</span>
                        @php 
                            $delete_url = route('st.delete',['id' => $u->id]);
                        @endphp
                        <span onclick="delete_record('{{ $delete_url }}')" >Delete</span>
                    </td>
                </tr>
                <!-- edit modal -->
                 <div class="modal fade" id="edit-modal-{{ $u->id }}">
                    <div class="modal-dialog">
                        <div class="modal-content">
                        
                            <!-- Modal Header -->
                            <div class="modal-header">
                            <h4 class="modal-title">Student</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            
                            <!-- Modal body -->
                            <div class="modal-body" data-modal="edit-modal-{{ $u->id }}">
                                <form class="uni-update" action="{{ route('st.add') }}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group">
                                        <label for="pwd">First Name:</label>
                                        <input type="text" class="form-control" id="name" placeholder="Enter First Name" name="first_name" value="{{ $u->first_name }}">
                                        <input type="hidden" name="id" value="{{ $u->id }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="pwd">Last Name:</label>
                                        <input type="text" class="form-control" id="name" placeholder="Enter Last Name" name="last_name" value="{{ $u->last_name }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Email:</label>
                                        <input type="email" class="form-control" id="email" placeholder="Enter email" name="email" value="{{ $u->email }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="email">University:</label>
                                        <select name="universitie_id"  class="form-control">
                                            @foreach($uni as $k => $un)
                                                <option value="{{ $un->id }}" @if($un->id == $u->universitie_id) Selected @endif>{{ $un->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Phone:</label>
                                        <input type="number" maxlength="10" class="form-control" id="file" placeholder="Enter Phone" name="phone" value="{{ $u->phone }}">
                                    </div>
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </form>
                            </div>
                            
                            <!-- Modal footer -->
                            <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <!--  -->
            @endforeach
            </tbody>
        </table>
        <div class="pagination-custom">
            {{ $student->links("pagination::bootstrap-4") }}
        </div>
        <!-- table end -->
        <!-- The Modal -->
        <div class="modal fade" id="myModal">
            <div class="modal-dialog">
                <div class="modal-content">
                
                    <!-- Modal Header -->
                    <div class="modal-header">
                    <h4 class="modal-title">Student</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    
                    <!-- Modal body -->
                    <div class="modal-body">
                        <form id="uni-add" action="{{ route('st.add') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="pwd">First Name:</label>
                                <input type="text" class="form-control" id="name" placeholder="Enter First Name" name="first_name">
                            </div>
                            <div class="form-group">
                                <label for="pwd">Last Name:</label>
                                <input type="text" class="form-control" id="name" placeholder="Enter Last Name" name="last_name">
                            </div>
                            <div class="form-group">
                                <label for="email">Email:</label>
                                <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
                            </div>
                            <div class="form-group">
                                <label for="email">University:</label>
                                <select name="universitie_id" class="form-control">
                                    @foreach($uni as $k => $un)
                                        <option value="{{ $un->id }}">{{ $un->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="email">Phone:</label>
                                <input type="number" maxlength="10" class="form-control" id="file" placeholder="Enter Phone" name="phone">
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                    
                    <!-- Modal footer -->
                    <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                    
                </div>
            </div>
        </div>
    
    </div>
    <script>
        $(document).ready(function(){
            $('form#uni-add').submit(function(e){
                e.preventDefault();
                ajax_run("{{ route('st.add') }}",'post',new FormData(this),'myModal');
            })
            $('form.uni-update').submit(function(e){
                e.preventDefault();
                var modal = $(this).parent().data('modal')
                ajax_run("{{ route('st.update') }}",'post',new FormData(this),modal);
            })
        })
        function edit(data)
        {
            console.log(JSON.parse(data));
        }
    </script>
@endsection
