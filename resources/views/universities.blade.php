@extends('layouts.main')

@section('content')
    <div class="container">
        <!-- Button to Open the Modal -->
        <div>
            <h2>University</h2>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                Add New
            </button>
        </div>
        <br />
        <!-- university tabel -->
        <div class="responsive">
            <table class="table">
                <thead>
                <tr>
                    <th>Logo</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th style="width:100px;">Website</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($unies as $kk => $u)
                    <tr>
                        <td>
                            <div class="image">
                                <img src="{{ $u->logo }}" height="100" width="100">
                            </div>
                        </td>
                        <td>{{$u->name}}</td>
                        <td>{{$u->email}}</td>
                        <td style="width:100px">{{$u->website}}</td>
                        <td>
                            <span data-toggle="modal" data-target="#edit-modal-{{ $u->id }}" >Edit</span>
                            @php 
                                $delete_url = route('uni.delete',['id' => $u->id]);
                            @endphp
                            <span onclick="delete_record('{{ $delete_url }}')" >Delete</span>
                        </td>
                    </tr>
                    <!-- edit modal -->
                    <div class="modal fade" id="edit-modal-{{ $u->id }}">
                        <div class="modal-dialog">
                            <div class="modal-content">
                            
                                <!-- Modal Header -->
                                <div class="modal-header">
                                <h4 class="modal-title">University</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                
                                <!-- Modal body -->
                                <div class="modal-body" data-modal="edit-modal-{{ $u->id }}">
                                    <form class="uni-update" action="{{ route('uni.update') }}" method="post" enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group">
                                            <label for="pwd">Name:</label>
                                            <input type="text" class="form-control" id="name" placeholder="Enter Name" name="name" value="{{$u->name}}">
                                            <input type="hidden" class="form-control" id="name" name="id" value="{{$u->id}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="email">Email:</label>
                                            <input type="email" class="form-control" id="email" placeholder="Enter email" name="email" value="{{$u->email}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="email">Website:</label>
                                            <input type="text" class="form-control" id="website" placeholder="Enter Website" name="website" value="{{$u->website}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="email">Logo:</label>
                                            <input type="file" class="form-control" id="file" placeholder="Select Image" name="logo" accept="image/*">
                                        </div>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </form>
                                </div>
                                
                                <!-- Modal footer -->
                                <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <!--  -->
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="pagination-custom">
            {{ $unies->links("pagination::bootstrap-4") }}
        </div>
        <!-- table end -->
        <!-- The Modal -->
        <div class="modal fade" id="myModal">
            <div class="modal-dialog">
                <div class="modal-content">
                
                    <!-- Modal Header -->
                    <div class="modal-header">
                    <h4 class="modal-title">University</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    
                    <!-- Modal body -->
                    <div class="modal-body">
                        <form id="uni-add" action="{{ route('uni.add') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="pwd">Name:</label>
                                <input type="text" class="form-control" id="name" placeholder="Enter Name" name="name">
                            </div>
                            <div class="form-group">
                                <label for="email">Email:</label>
                                <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
                            </div>
                            <div class="form-group">
                                <label for="email">Website:</label>
                                <input type="text" class="form-control" id="website" placeholder="Enter Website" name="website">
                            </div>
                            <div class="form-group">
                                <label for="email">Logo:</label>
                                <input type="file" class="form-control" id="file" placeholder="Select Image" name="logo" accept="image/*">
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                    
                    <!-- Modal footer -->
                    <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                    
                </div>
            </div>
        </div>
    
    </div>
    <script>
        $(document).ready(function(){
            $('form#uni-add').submit(function(e){
                e.preventDefault();
                ajax_run("{{ route('uni.add') }}",'post',new FormData(this),'myModal');
            })
            $('form.uni-update').submit(function(e){
                e.preventDefault();
                var modal = $(this).parent().data('modal')
                ajax_run("{{ route('uni.update') }}",'post',new FormData(this),modal);
            })
        })
        function edit(data)
        {
            console.log(JSON.parse(data));
        }
    </script>
@endsection
